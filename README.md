# Générer les clés et les certificats

## Installer easy rsa

`sudo apt install easy-rsa`

Créer du répertoire qui va contenir tous les outils :
`make-cadir openvpn-ca`

`cd openvpn-ca `

Exporter les variables d'environnement nécessaires à la suite de la procedure :

`source ./vars`

Remarque : on peut modifier ce fichier si on veut changer la date d'expiration et d'autres paramètres.

Nettoyer les clés qui seraient déjà présentes :

` ./clean-all`

## Certificat Ca root
`./build-ca`

## Certificat serveur
`./build-key-server server`

## Clef Diffie-Hellman
`./build-dh`

## Générer le certificat client:
`./build-key client1`

Si vous voulez rajoutez un mot de passe faire : `./build-key-pass client1`

# Configuration client
Afin de se connecter à notre VPN, il faut d'abord créer une conguration client : `client.ovpn`.

Générer et copier le certificat CA (`ca.crt`), le certificat client (`client.crt`) et la clé privée client (`client.key`) dans le même répertoire.

Dans le fichier `client.ovpn`, coller:
```
client
cipher AES-128-CBC
nobind
dev tun
redirect-gateway def1 bypass-dhcp

ca ca.crt
cert client.crt
key client.key

remote <IP_DE_VOTRE_SERVEUR> 1194 udp
```

En remplaçant `<IP_DE_VOTRE_SERVEUR>` par l'IP publique de votre serveur VPN.

# Docker
## Générer l'image Docker

Se placer dans le répertoire contenant le Dockerfile.

Copier le certificat CA (`ca.crt`), le certificat serveur (`server.crt`), la clé privée serveur (`server.key`) et la clé de Diffie-Hellman (`dh2048.pem`) dans ce répertoire, puis :

`sudo docker build -t openvpn .`

## Lancer un container

Pour lancer le VPN avec les configurations par défaut :

`sudo docker run --name openvpn_1 --privileged -p 1194:1194/udp openvpn`

Plusieurs paramètres peuvent être spécifiés via des variables d'environnement (`-e`):
- `LOCAL_IP` : IP du réseau local au VPN [défaut: 10.8.0.0]
- `PROTO` : Protocole utilisé (ne pas oublier de modifier l'argument `-p` de `docker run`) (ne pas oublier de modifier `client.ovpn`) [défaut: udp] 
- `FORWARD_INTERFACE` : Interface réseau utilisée pour rediriger les packets vers internet [défaut: eth0]

Exemple d'utilisation :

`sudo docker run --name openvpn_1 --privileged -p 1194:1194/tcp -e LOCAL_IP=10.8.9.0 -e PROTO=tcp -e FORWARD_INTERFACE=eth0 openvpn`
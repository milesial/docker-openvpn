FROM debian:jessie-slim

# Dependencies
RUN apt-get update
RUN apt-get install -y openvpn iptables

ENV LOCAL_IP=10.8.0.0
ENV PROTO=udp
ENV FORWARD_INTERFACE=eth0

EXPOSE 1194/${PROTO}

# Main script
COPY start /

# Certificates and keys
COPY dh2048.pem /etc/openvpn/
COPY server.key /etc/openvpn/
COPY server.crt /etc/openvpn/
COPY ca.crt /etc/openvpn/

# Cleaning
RUN apt-get clean
RUN rm -rf /var/lib/apt/lists/*
RUN rm -rf /usr/share/doc/*
RUN rm -rf /tmp/*

# Set NAT forwarding, set openvpn config and start it
CMD ["/start"]
